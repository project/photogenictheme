var top_menu_height = 0;
jQuery(function($) {
    jQuery(window).on('load', function(){
            $('.external-link').unbind('click');    
        });
        
        jQuery(document).ready( function() {

        // scroll to specific id when click on menu
        jQuery('.templatemo-top-menu .navbar-nav a').click(function(e){
            e.preventDefault(); 
            var linkId = $(this).attr('href');
            scrollTo(linkId);
            if($('.navbar-toggle').is(":visible") == true){
                jQuery('.navbar-collapse').collapse('toggle');
            }
            jQuery(this).blur();
            return false;
        });

        // gallery category
        jQuery('.templatemo-gallery-category a').click(function(e){
            e.preventDefault(); 
            jQuery(this).parent().children('a').removeClass('active');
            jQuery(this).addClass('active');
            var linkClass = jQuery(this).attr('href');
            jQuery('.gallery').each(function(){
                if(jQuery(this).is(":visible") == true){
                    jQuery(this).hide();
                };
            });
            jQuery(linkClass).fadeIn();  
        });

        //gallery light box setup
        jQuery('a.colorbox').colorbox({
            rel: function(){
            return jQuery(this).data('group');
        }
        });
    });
});

// scroll animation 
function scrollTo(selectors)
{
    if(!jQuery(selectors).size()) return;
    var selector_top = jQuery(selectors).offset().top - top_menu_height;
    jQuery('html,body').animate({ scrollTop: selector_top }, 'slow');
}
